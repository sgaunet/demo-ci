package main

import (
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"
)

func helloServer(w http.ResponseWriter, req *http.Request) {

	entries, _ := os.ReadDir("images/")
	var images []string

	for _, entry := range entries {
		images = append(images, entry.Name())
	}

	if req.Method == "GET" {
		buf, err := os.ReadFile("images/" + images[rand.Intn(len(images)-1)])

		if err != nil {
			log.Fatal(err)
		}

		w.Header().Set("Content-Type", "image/png")
		w.Write(buf)
	} else {
		w.WriteHeader(http.StatusForbidden)
	}
}

func main() {
	http.HandleFunc("/", helloServer)
	server := &http.Server{
		Addr:              ":8880",
		ReadHeaderTimeout: 3 * time.Second,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
