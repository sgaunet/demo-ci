# ko.build

## Information

- [Official website](https://ko.build/)
- Installation

```bash
brew install ko
```

## Build locally

```bash
ko build -L .
```

## Publish to a registry

Need to setup registry information, here on GitLab

```bash
export export KO_DOCKER_REPO=registry.gitlab.com/yodamad-workshops/devops-dday-2023/demo-ci
export GITLAB_TOKEN=xxx
```

We can configure ko behavior in a `.ko.yaml` at the root level of the repository. For instance, we can specify which are the targeted platform.

```yaml
defaultPlatforms:
- linux/amd64
```

```bash
ko build -B .
```

ℹ️ : You can specify several platforms, but not all registries support multi-platform images
