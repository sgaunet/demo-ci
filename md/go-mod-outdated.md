# go-mod-outdated

## Information

- [Official site](https://github.com/psampaz/go-mod-outdated)
- Installation

```bash
go install github.com/psampaz/go-mod-outdated@latest
```

## Run

```bash
go list -u -m -json all | go-mod-outdated
```

*Output*

```
+-----------------------------------------+------------------------------------+-------------+--------+------------------+
|                 MODULE                  |              VERSION               | NEW VERSION | DIRECT | VALID TIMESTAMPS |
+-----------------------------------------+------------------------------------+-------------+--------+------------------+
| github.com/Masterminds/semver/v3        | v3.1.1                             | v3.2.1      | false  | true             |
| github.com/Unleash/unleash-client-go/v3 | v3.7.4                             | v3.9.0      | true   | true             |
| github.com/davecgh/go-spew              | v1.1.1                             |             | false  | true             |
| github.com/nbio/st                      | v0.0.0-20140626010706-e9e8d9816f32 |             | false  | true             |
| github.com/pmezard/go-difflib           | v1.0.0                             |             | false  | true             |
| github.com/stretchr/objx                | v0.1.1                             | v0.5.1      | false  | true             |
| github.com/stretchr/testify             | v1.2.2                             | v1.8.4      | false  | true             |
| github.com/twmb/murmur3                 | v1.1.5                             | v1.1.8      | false  | true             |
| gopkg.in/h2non/gock.v1                  | v1.0.10                            | v1.1.2      | false  | true             |
+-----------------------------------------+------------------------------------+-------------+--------+------------------+
```

## Run in CI

To run in a CI and detect problems, add `-ci` argument to command

```bash
go list -u -m -json all | go-mod-outdated -ci
```

And if there are some modules to update, return code will become `1` and easily detected in a CI job to fail
