# Goreleaser

## Information

- [Official website](https://goreleaser.com/)
- Installation

```bash
go install github.com/goreleaser/goreleaser@latest
```

- Init project

```bash
goreleaser init
```

## Dry-run local

### Basic

```bash
goreleaser build --single-target --clean --snapshot
```

```
  • starting build...
  • could not find a configuration file, using defaults...
  • loading                                          path=
  • building only for darwin/amd64                   reason=single target is enabled
  • skipping validate...
  • loading environment variables
    • using token from  $GITLAB_TOKEN
  • getting and validating git state
    • ignoring errors because this is a snapshot     error=git doesn't contain any tags. Either add a tag or use --snapshot
    • git state                                      commit=879cdf666d9c27cdf16054d2226e7aa099ca28b8 branch=main current_tag=v0.0.0 previous_tag=<unknown> dirty=false
    • pipe skipped                                   reason=disabled during snapshot mode
  • parsing tag
  • setting defaults
  • snapshotting
    • building snapshot...                           version=0.0.0-SNAPSHOT-879cdf6
  • checking distribution directory
    • cleaning dist
  • loading go mod information
  • build prerequisites
  • writing effective config file
    • writing                                        config=dist/config.yaml
  • building binaries
    • building                                       binary=dist/demo-ci_darwin_amd64_v1/demo-ci
    • took: 3s
  • storing release metadata
    • writing                                        file=dist/artifacts.json
    • writing                                        file=dist/metadata.json
  • build succeeded after 3s
  • thanks for using goreleaser!
```

### With some configuration

We can set up some configuration to define platforms, release bundles properties, ...

```yaml
before:
  hooks:
    # You may remove this if you don't use go modules.
    - go mod tidy

builds:
  - goos:
      - linux
      - windows
    goarch:
      - 386
      - amd64

archives:
  - format: tar.gz
    # this name template makes the OS and Arch compatible with the results of `uname`.
    name_template: >-
      {{ .ProjectName }}_
      {{- title .Os }}_
      {{- if eq .Arch "amd64" }}x86_64
      {{- else if eq .Arch "386" }}i386
      {{- else }}{{ .Arch }}{{ end }}
      {{- if .Arm }}v{{ .Arm }}{{ end }}
    # use zip for windows archives
    format_overrides:
      - goos: windows
        format: zip

changelog:
  sort: asc
  filters:
    exclude:
      - "^docs:"
      - "^test:"

release:
  gitlab:
    owner: yodamad-workshops/devops-dday-2023
    name: demo

```

*Run command with configuration*

```bash
goreleaser build --clean --snapshot -f conf/goreleaser.yml
```

*Output*

```
  • starting build...
  • loading                                          path=conf/goreleaser.yml
  • skipping validate...
  • loading environment variables
    • using token from  $GITLAB_TOKEN
  • getting and validating git state
    • ignoring errors because this is a snapshot     error=git doesn't contain any tags. Either add a tag or use --snapshot
    • git state                                      commit=879cdf666d9c27cdf16054d2226e7aa099ca28b8 branch=main current_tag=v0.0.0 previous_tag=<unknown> dirty=false
    • pipe skipped                                   reason=disabled during snapshot mode
  • parsing tag
  • setting defaults
  • snapshotting
    • building snapshot...                           version=0.0.0-SNAPSHOT-879cdf6
  • running before hooks
    • running                                        hook=go mod tidy
  • checking distribution directory
    • cleaning dist
  • loading go mod information
  • build prerequisites
  • writing effective config file
    • writing                                        config=dist/config.yaml
  • building binaries
    • building                                       binary=dist/demo_windows_amd64_v1/demo.exe
    • building                                       binary=dist/demo_linux_amd64_v1/demo
    • building                                       binary=dist/demo_windows_386/demo.exe
    • building                                       binary=dist/demo_linux_386/demo
    • took: 17s
  • storing release metadata
    • writing                                        file=dist/artifacts.json
    • writing                                        file=dist/metadata.json
  • build succeeded after 17s
  • thanks for using goreleaser!
```

## Release

First we need to tag

```bash
git tag v1.0
```

### Basic

```bash
goreleaser release --clean -f conf/goreleaser.yml
```

*Output*

```
  • starting release...
  • loading                                          path=conf/goreleaser.yml
  • loading environment variables
    • using token from  $GITLAB_TOKEN
  • getting and validating git state
    • git state                                      commit=8f0881ab8a5580d0153c693f974b5dafee6f2332 branch=main current_tag=v1.6 previous_tag=v1.5 dirty=false
  • parsing tag
  • setting defaults
  • running before hooks
    • running                                        hook=go mod tidy
  • checking distribution directory
    • cleaning dist
  • loading go mod information
  • build prerequisites
  • writing effective config file
    • writing                                        config=dist/config.yaml
  • building binaries
    • building                                       binary=dist/demo-ci_windows_amd64_v1/demo-ci.exe
    • building                                       binary=dist/demo-ci_linux_386/demo-ci
    • building                                       binary=dist/demo-ci_linux_amd64_v1/demo-ci
    • building                                       binary=dist/demo-ci_windows_386/demo-ci.exe
    • took: 1s
  • generating changelog
    • writing                                        changelog=dist/CHANGELOG.md
  • archives
    • creating                                       archive=dist/demo-ci_Linux_x86_64.tar.gz
    • creating                                       archive=dist/demo-ci_Linux_i386.tar.gz
    • creating                                       archive=dist/demo-ci_Windows_i386.zip
    • creating                                       archive=dist/demo-ci_Windows_x86_64.zip
    • took: 1s
  • calculating checksums
  • publishing
    • scm releases
      • creating or updating release                 tag=v1.6 repo=
      • refreshing checksums                         file=demo-ci_1.6_checksums.txt
      • release created                              name=v1.6
      • uploading to release                         file=dist/demo-ci_1.6_checksums.txt name=demo-ci_1.6_checksums.txt
      • uploading to release                         file=dist/demo-ci_Windows_i386.zip name=demo-ci_Windows_i386.zip
      • uploading to release                         file=dist/demo-ci_Linux_i386.tar.gz name=demo-ci_Linux_i386.tar.gz
      • uploading to release                         file=dist/demo-ci_Windows_x86_64.zip name=demo-ci_Windows_x86_64.zip
      • uploading to release                         file=dist/demo-ci_Linux_x86_64.tar.gz name=demo-ci_Linux_x86_64.tar.gz
      • published                                    url=https://gitlab.com/yodamad-workshops/devops-dday-2023/demo-ci/-/releases/v1.6
      • took: 19s
  • took: 19s
  • storing release metadata
    • writing                                        file=dist/artifacts.json
    • writing                                        file=dist/metadata.json
  • announcing
  • release succeeded after 21s
  • thanks for using goreleaser!
```

### With ko.build

We can setup goreleaer to use `ko.build` to build 🐳 images when releasing.
We update the previous configuration ([goreleaser_v2.yml](../conf/goreleaser_v2.yml)).

```yaml
hooks:
    # You may remove this if you don't use go modules.
    - go mod tidy

builds:
  - env:
      - CGO_ENABLED=0
    goos:
      - linux
      - windows
    goarch:
      - 386
      - amd64

archives:
  - format: tar.gz
    # this name template makes the OS and Arch compatible with the results of `uname`.
    name_template: >-
      {{ .ProjectName }}_
      {{- title .Os }}_
      {{- if eq .Arch "amd64" }}x86_64
      {{- else if eq .Arch "386" }}i386
      {{- else }}{{ .Arch }}{{ end }}
      {{- if .Arm }}v{{ .Arm }}{{ end }}
    # use zip for windows archives
    format_overrides:
      - goos: windows
        format: zip

changelog:
  sort: asc
  filters:
    exclude:
      - "^docs:"
      - "^test:"

release:
  gitlab:
    owner: yodamad-workshops/devops-dday-2023
    name: demo

kos:
  - repository: registry.gitlab.com/yodamad-workshops/devops-dday-2023/demo/go-sample
    tags:
    - '{{.Version}}'
    - latest
    bare: true
    preserve_import_paths: false
    platforms:
    - linux/amd64
    - linux/arm64

```

*Run command with configuration*

```bash
goreleaser release --clean -f conf/goreleaser_v2.yml
```

*Output*

```
  • starting release...
  • loading                                          path=conf/goreleaser_v2.yml
  • loading environment variables
    • using token from  $GITLAB_TOKEN
  • getting and validating git state
    • git state                                      commit=2a48430ab194cace5017882f85218fd34d46a073 branch=main current_tag=v1.7 previous_tag=v1.6 dirty=false
  • parsing tag
  • setting defaults
  • running before hooks
    • running                                        hook=go mod tidy
  • checking distribution directory
    • cleaning dist
  • loading go mod information
  • build prerequisites
  • writing effective config file
    • writing                                        config=dist/config.yaml
  • building binaries
    • building                                       binary=dist/demo-ci_linux_amd64_v1/demo-ci
    • building                                       binary=dist/demo-ci_windows_amd64_v1/demo-ci.exe
    • building                                       binary=dist/demo-ci_windows_386/demo-ci.exe
    • building                                       binary=dist/demo-ci_linux_386/demo-ci
    • took: 2s
  • generating changelog
    • writing                                        changelog=dist/CHANGELOG.md
  • archives
    • creating                                       archive=dist/demo-ci_Windows_x86_64.zip
    • creating                                       archive=dist/demo-ci_Linux_x86_64.tar.gz
    • creating                                       archive=dist/demo-ci_Linux_i386.tar.gz
    • creating                                       archive=dist/demo-ci_Windows_i386.zip
    • took: 1s
  • calculating checksums
  • publishing
    • ko
2023/11/14 16:37:53 Building go-sample for linux/amd64
2023/11/14 16:37:54 Publishing registry.gitlab.com/yodamad-workshops/devops-dday-2023/demo-ci:1.7
2023/11/14 16:37:54 Tagging registry.gitlab.com/yodamad-workshops/devops-dday-2023/demo-ci:latest
2023/11/14 16:38:00 Published SBOM registry.gitlab.com/yodamad-workshops/devops-dday-2023/demo-ci:sha256-cbbeae8b8cc1752d40eec22306e393b54d319b6a5f212ad4c3534fc0db9647a7.sbom
2023/11/14 16:38:09 Published registry.gitlab.com/yodamad-workshops/devops-dday-2023/demo-ci@sha256:cbbeae8b8cc1752d40eec22306e393b54d319b6a5f212ad4c3534fc0db9647a7
      • took: 20s
    • scm releases
      • creating or updating release                 tag=v1.7 repo=
      • refreshing checksums                         file=demo-ci_1.7_checksums.txt
      • release created                              name=v1.7
      • uploading to release                         file=dist/demo-ci_Linux_i386.tar.gz name=demo-ci_Linux_i386.tar.gz
      • uploading to release                         file=dist/demo-ci_Windows_i386.zip name=demo-ci_Windows_i386.zip
      • uploading to release                         file=dist/demo-ci_Linux_x86_64.tar.gz name=demo-ci_Linux_x86_64.tar.gz
      • uploading to release                         file=dist/demo-ci_1.7_checksums.txt name=demo-ci_1.7_checksums.txt
      • uploading to release                         file=dist/demo-ci_Windows_x86_64.zip name=demo-ci_Windows_x86_64.zip
      • published                                    url=https://gitlab.com/yodamad-workshops/devops-dday-2023/demo-ci/-/releases/v1.7
      • took: 5s
  • took: 24s
  • storing release metadata
    • writing                                        file=dist/artifacts.json
    • writing                                        file=dist/metadata.json
  • announcing
  • release succeeded after 28s
  • thanks for using goreleaser!
```
