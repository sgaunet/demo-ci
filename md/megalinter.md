# Megalinter

## Information

- [Official site](https://megalinter.io/latest/)

No need to install any CLI, but it requires [npx](https://www.npmjs.com/package/npx)

## Run

### Default

```bash
npx mega-linter-runner -e 'SHOW_ELAPSED_TIME=true'
```

**=> Very very long execution**

```
Pulling docker image oxsecurity/megalinter:v7 ...
INFO: this operation can be long during the first use of mega-linter-runner
The next runs, it will be immediate (thanks to docker cache !)
v7: Pulling from oxsecurity/megalinter
[...]
+----SUMMARY----+--------------------------+---------------+-------+-------+--------+--------------+
| Descriptor    | Linter                   | Mode          | Files | Fixed | Errors | Elapsed time |
+---------------+--------------------------+---------------+-------+-------+--------+--------------+
| ✅ BASH       | bash-exec                | file          |     1 |       |      0 |        0.01s |
| ❌ BASH       | shellcheck               | list_of_files |     1 |       |      3 |        0.21s |
| ✅ BASH       | shfmt                    | list_of_files |     1 |       |      0 |        0.06s |
| ✅ COPYPASTE  | jscpd                    | project       |   n/a |       |      0 |        3.51s |
| ✅ GO         | golangci-lint            | list_of_files |     1 |       |      0 |       43.18s |
| ❌ GO         | revive                   | list_of_files |     1 |       |      1 |       25.34s |
| ⚠️ MARKDOWN   | markdownlint             | list_of_files |     4 |       |     28 |        1.78s |
| ✅ MARKDOWN   | markdown-link-check      | list_of_files |     4 |       |      0 |        5.47s |
| ✅ MARKDOWN   | markdown-table-formatter | list_of_files |     4 |       |      0 |        1.43s |
| ✅ REPOSITORY | checkov                  | project       |   n/a |       |      0 |       44.89s |
| ✅ REPOSITORY | devskim                  | project       |   n/a |       |      0 |         6.6s |
| ✅ REPOSITORY | dustilock                | project       |   n/a |       |      0 |        0.07s |
| ❌ REPOSITORY | gitleaks                 | project       |   n/a |       |      2 |        0.68s |
| ✅ REPOSITORY | git_diff                 | project       |   n/a |       |      0 |         0.2s |
| ✅ REPOSITORY | grype                    | project       |   n/a |       |      0 |       39.87s |
| ✅ REPOSITORY | kics                     | project       |   n/a |       |      0 |       54.41s |
| ✅ REPOSITORY | secretlint               | project       |   n/a |       |      0 |        2.12s |
| ✅ REPOSITORY | syft                     | project       |   n/a |       |      0 |         2.5s |
| ✅ REPOSITORY | trivy                    | project       |   n/a |       |      0 |       27.84s |
| ✅ REPOSITORY | trivy-sbom               | project       |   n/a |       |      0 |       27.44s |
| ❌ REPOSITORY | trufflehog               | project       |   n/a |       |      1 |       17.68s |
| ❌ SPELL      | cspell                   | list_of_files |    12 |       |     83 |       23.44s |
| ✅ SPELL      | lychee                   | list_of_files |     9 |       |      0 |        0.95s |
| ⚠️ YAML       | prettier                 | list_of_files |     5 |       |      1 |         3.0s |
| ✅ YAML       | v8r                      | list_of_files |     5 |       |      0 |        9.01s |
| ❌ YAML       | yamllint                 | list_of_files |     5 |       |      1 |        1.91s |
+---------------+--------------------------+---------------+-------+-------+--------+--------------+
```

### Focus on Golang

Megalinter introduces flavors to optimize run for dedicated langages.

```bash
npx mega-linter-runner --flavor go -e ENABLE=GO -e 'SHOW_ELAPSED_TIME=true'
```

**=> Quicker execution**

```
+----SUMMARY-+---------------+---------------+-------+-------+--------+--------------+
| Descriptor | Linter        | Mode          | Files | Fixed | Errors | Elapsed time |
+------------+---------------+---------------+-------+-------+--------+--------------+
| ✅ GO      | golangci-lint | list_of_files |     1 |       |      0 |       19.23s |
| ❌ GO      | revive        | list_of_files |     1 |       |      1 |        14.4s |
+------------+---------------+---------------+-------+-------+--------+--------------+
```

### Optimized

Remove duplicate execution of Revive, use advanced configuration of GolangCI-lint ([megalinter.yml](../conf/megalinter.yml))

```yml
ENABLE:
  - GO

DISABLE_LINTERS: GO_REVIVE

GO_GOLANGCI_LINT_CONFIG_FILE: conf/golangci-lint_advanced.yml
```

*Run command*

```bash
npx mega-linter-runner --flavor go -e EXTENDS=conf/megalinter.yml -e 'SHOW_ELAPSED_TIME=true'
```

*Optimized output*

```
- MegaLinter key: [GO_GOLANGCI_LINT]
- Rules config: [/conf/golangci-lint_advanced.yml]
- Number of files analyzed: [1]
--Error detail:
main.go:11:18  varnamelen     parameter name 'w' is too short for the scope of its usage
main.go:11:60  revive         empty-lines: extra empty line at the start of a block
main.go:11:60  wsl            block should not start with a whitespace
main.go:11     whitespace     unnecessary leading newline
main.go:12     gofumpt        File is not `gofumpt`-ed
main.go:14:2   prealloc       Consider pre-allocating `images`
main.go:14:2   wsl            declarations should never be cuddled
main.go:20:19  usestdlibvars  "GET" can be replaced by http.MethodGet
main.go:21:46  gosec          G404: Use of weak random number generator (math/rand instead of crypto/rand)
main.go:22     gofumpt        File is not `gofumpt`-ed
main.go:24:4   revive         deep-exit: calls to log.Fatal only in main() or init() functions
main.go:28:3   revive         unhandled-error: Unhandled error in call to function w.Write
main.go:28:10  errcheck       Error return value of `w.Write` is not checked
main.go:36:2   wsl            assignments should only be cuddled with other assignments
main.go:36:13  exhaustruct    http.Server is missing fields Handler, DisableGeneralOptionsHandler, TLSConfig, ReadTimeout, WriteTimeout, IdleTimeout, MaxHeaderBytes, TLSNextProto, ConnState, ErrorLog, BaseContext, ConnContext
main.go:38:22  revive         add-constant: avoid magic numbers like '3', create a named constant for it
main.go:38:22  gomnd          mnd: Magic number: 3, in <assign> detected



+----SUMMARY-+---------------+---------------+-------+-------+--------+--------------+
| Descriptor | Linter        | Mode          | Files | Fixed | Errors | Elapsed time |
+------------+---------------+---------------+-------+-------+--------+--------------+
| ❌ GO      | golangci-lint | list_of_files |     1 |       |      1 |       12.53s |
+------------+---------------+---------------+-------+-------+--------+--------------+
```
