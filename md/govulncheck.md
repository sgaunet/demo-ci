# Govulncheck

## Information

- [Official website](https://pkg.go.dev/golang.org/x/vuln/cmd/govulncheck)
- Installation

```bash
go install golang.org/x/vuln/cmd/govulncheck@latest
```

## Run

```bash
govulncheck ./...
```

*Output*

```
Scanning your code and 129 packages across 0 dependent modules for known vulnerabilities...

=== Informational ===

Found 2 vulnerabilities in packages that you import, but there are no call
stacks leading to the use of these vulnerabilities. You may not need to
take any action. See https://pkg.go.dev/golang.org/x/vuln/cmd/govulncheck
for details.

Vulnerability #1: GO-2023-2186
    Incorrect detection of reserved device names on Windows in path/filepath
  More info: https://pkg.go.dev/vuln/GO-2023-2186
  Standard library
    Found in: path/filepath@go1.21.3
    Fixed in: path/filepath@go1.21.4

Vulnerability #2: GO-2023-2185
    Insecure parsing of Windows paths with a \??\ prefix in path/filepath
  More info: https://pkg.go.dev/vuln/GO-2023-2185
  Standard library
    Found in: internal/safefilepath@go1.21.3
    Fixed in: internal/safefilepath@go1.21.4
    Platforms: windows
```