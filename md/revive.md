# Revive

## Information

- [Official site](https://github.com/mgechev/revive)
- Installation

```bash
go install github.com/mgechev/revive@latest
```

## Run

### Default

```bash
revive
``````
 
**=> Poor output**
```
main.go:1:1: should have a package comment
```

### Enable all

Set up some config to enable all rules ([revive.toml](../conf/revive.toml))

*Configuration file*

```toml
enableAllRules = true
```

*Run command*
```bash
revive -config conf/revive.toml
```
 
**=> Rich output but some are useless**

```
main.go:11:60: extra empty line at the start of a block
main.go:38:22: avoid magic numbers like '3', create a named constant for it
main.go:28:3: Unhandled error in call to function w.Write
main.go:24:4: calls to log.Fatal only in main() or init() functions
main.go:1:1: should have a package comment
```

### Enable all & disable some rules

Set up some config to enable all rules ([revive.toml](../conf/revive_v2.toml))

*Configuration file*

```toml
enableAllRules = true

[rule.line-length-limit]
Disabled = true

```

*Run command*
```bash
revive -config conf/revive_v2.toml
```
 
**=> Optimized output**

```
main.go
  (1, 1)    https://revive.run/r#package-comments  should have a package comment
  (38, 22)  https://revive.run/r#add-constant      avoid magic numbers like '3', create a named constant for it
  (11, 60)  https://revive.run/r#empty-lines       extra empty line at the start of a block
  (24, 4)   https://revive.run/r#deep-exit         calls to log.Fatal only in main() or init() functions
  (28, 3)   https://revive.run/r#unhandled-error   Unhandled error in call to function w.Write


 ✖ 5 problems (0 errors) (5 warnings)
```