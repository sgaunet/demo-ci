# SonarQube

- Install a SonarQube install and create a project manually on it to get a token.
- Install sonar-scanner : `brew install sonar-scanner`
- Export token to env. variable : 

```bash
export SONAR_TOKEN=xxxxx
```

## Configure

Create a `sonar-project.properties` file as the root level of the repository

```properties
sonar.host.url=https://aphrodite.yodamad.fr
sonar.projectKey=demo-ci
sonar.qualitygate.wait=true
``````

## First run

*Run command*

```bash
sonar-scanner
```

No result 🥲

## Second run

Configure SonarQube to know about GolangCI-lint in `Project Settings > General Settings > Languages > Go > GolangCI-Lint Report Files` and set value to `dist/golangci-lint.xml`.

Now we need to generate a GolangCI-lint result file compatible with SonarQube, based on Checkstyle format.

```bash
golangci-lint run -c conf/golangci-lint_advanced.yml --out-format checkstyle > dist/golangci-lint.xml
```

Now we can re-run the sonar command

```bash
sonar-scanner
```

And the magic happens 🪄, results appear in SonarQube
