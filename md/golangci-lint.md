# GolangCI-lint

## Information

- [Official site](https://github.com/golangci/golangci-lint)
- Installation

```bash
go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.55.2
```

## Run

### Default

```bash
golangci-lint run
```

**=> Poor output**
```
main.go:28:10: Error return value of `w.Write` is not checked (errcheck)
		w.Write(buf)
		       ^
```

### Enable all

Set up some config to enable all rules ([golangci-lint.yml](../conf/golangci-lint.yml))

*Configuration file*

```yml
linters:
  enable-all: true
```

*Run command*
```bash
golangci-lint run -c conf/golangci-lint.yml
```
 
**=> Rich output but pollute with warning and lot of information**

```
WARN [runner] The linter 'scopelint' is deprecated (since v1.39.0) due to: The repository of the linter has been deprecated by the owner. Replaced by exportloopref.
WARN [runner] The linter 'golint' is deprecated (since v1.41.0) due to: The repository of the linter has been archived by the owner. Replaced by revive.
WARN [runner] The linter 'deadcode' is deprecated (since v1.49.0) due to: The owner seems to have abandoned the linter. Replaced by unused.
WARN [runner] The linter 'structcheck' is deprecated (since v1.49.0) due to: The owner seems to have abandoned the linter. Replaced by unused.
WARN [runner] The linter 'varcheck' is deprecated (since v1.49.0) due to: The owner seems to have abandoned the linter. Replaced by unused.
WARN [runner] The linter 'nosnakecase' is deprecated (since v1.48.1) due to: The repository of the linter has been deprecated by the owner. Replaced by revive(var-naming).
WARN [runner] The linter 'exhaustivestruct' is deprecated (since v1.46.0) due to: The owner seems to have abandoned the linter. Replaced by exhaustruct.
WARN [runner] The linter 'maligned' is deprecated (since v1.38.0) due to: The repository of the linter has been archived by the owner. Replaced by govet 'fieldalignment'.
WARN [runner] The linter 'ifshort' is deprecated (since v1.48.0) due to: The repository of the linter has been deprecated by the owner.
WARN [runner] The linter 'interfacer' is deprecated (since v1.38.0) due to: The repository of the linter has been archived by the owner.
main.go:28:10: Error return value of `w.Write` is not checked (errcheck)
		w.Write(buf)
		       ^
main.go:12: File is not `gofumpt`-ed (gofumpt)

main.go:22: File is not `gofumpt`-ed (gofumpt)

main.go:21:46: G404: Use of weak random number generator (math/rand instead of crypto/rand) (gosec)
		buf, err := os.ReadFile("images/" + images[rand.Intn(len(images)-1)])
		                                           ^
main.go:14:2: Consider pre-allocating `images` (prealloc)
	var images []string
	^
main.go:11: unnecessary leading newline (whitespace)
func helloServer(w http.ResponseWriter, req *http.Request) {

main.go:36:2: assignments should only be cuddled with other assignments (wsl)
	server := &http.Server{
	^
main.go:38:22: mnd: Magic number: 3, in <assign> detected (gomnd)
		ReadHeaderTimeout: 3 * time.Second,
		                   ^
main.go:41:2: variable 'err' is only used in the if-statement (main.go:42:2); consider using short syntax (ifshort)
	err := server.ListenAndServe()
	^
main.go:20:19: "GET" can be replaced by http.MethodGet (usestdlibvars)
	if req.Method == "GET" {
```

### Optimized

Enable rules, format output like sorted lines, print linter, ... ([golangci-lint_advanced.yml](../conf/golangci-lint_advanced.yml))

```yml
linters:
  enable-all: true
  disable:
    - golint
    - scopelint
    - interfacer
    - varcheck
    - exhaustivestruct
    - ifshort
    - deadcode
    - maligned
    - structcheck
    - nosnakecase

output:
  format: colored-tab
  print-linter-name: true
  uniq-by-line: false
  sort-results: true

linters-settings:
  revive:
    enable-all-rules: true
``````

*Run command*
```bash
golangci-lint run -c conf/golangci-lint_advanced.yml
```

*Optimized output*
```
main.go:11:18  varnamelen     parameter name 'w' is too short for the scope of its usage
main.go:11:60  revive         empty-lines: extra empty line at the start of a block
main.go:11:60  wsl            block should not start with a whitespace
main.go:11     whitespace     unnecessary leading newline
main.go:12     gofumpt        File is not `gofumpt`-ed
main.go:14:2   prealloc       Consider pre-allocating `images`
main.go:14:2   wsl            declarations should never be cuddled
main.go:20:19  usestdlibvars  "GET" can be replaced by http.MethodGet
main.go:21:46  gosec          G404: Use of weak random number generator (math/rand instead of crypto/rand)
main.go:22     gofumpt        File is not `gofumpt`-ed
main.go:24:4   revive         deep-exit: calls to log.Fatal only in main() or init() functions
main.go:28:3   revive         unhandled-error: Unhandled error in call to function w.Write
main.go:28:10  errcheck       Error return value of `w.Write` is not checked
main.go:36:2   wsl            assignments should only be cuddled with other assignments
main.go:36:13  exhaustruct    http.Server is missing fields Handler, DisableGeneralOptionsHandler, TLSConfig, ReadTimeout, WriteTimeout, IdleTimeout, MaxHeaderBytes, TLSNextProto, ConnState, ErrorLog, BaseContext, ConnContext
main.go:38:22  revive         add-constant: avoid magic numbers like '3', create a named constant for it
main.go:38:22  gomnd          mnd: Magic number: 3, in <assign> detected
```
