#!/usr/bin/zsh

echo "💉 Sourcing aliases"
source zsh/demo-ci.source
echo "🤐 Sourcing secrets"
source zsh/mvt.source
echo "🔧 Sourcing alias tab completion"
source zsh/alt-zshrc.source
echo "✅ All done"
