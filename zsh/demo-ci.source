# Revive aliases

alias revive_install="go install github.com/mgechev/revive@latest"

alias revive_v1="revive"
alias revive_v2="revive -config conf/revive.toml"
alias revive_v3="revive -config conf/revive_v2.toml -formatter stylish"

# golangci-lint aliases

alias golangci-lint_install="go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.55.0"

alias golangci-lint_v1="golangci-lint run"
alias golangci-lint_v2="golangci-lint run -c conf/golangci-lint.yml"
alias golangci-lint_v3="golangci-lint run -c conf/golangci-lint_advanced.yml"
alias golangci-lint_sonar="golangci-lint run -c conf/golangci-lint_advanced.yml --out-format checkstyle > dist/golangci-lint.xml"

# megalinter aliases

alias megalinter_default="npx mega-linter-runner -e 'SHOW_ELAPSED_TIME=true'"
alias megalinter_go="npx mega-linter-runner --flavor go -e "'ENABLE=GO'" -e 'SHOW_ELAPSED_TIME=true'"
alias megalinter_optimized="npx mega-linter-runner --flavor go -e "'EXTENDS=conf/megalinter.yml'" -e 'SHOW_ELAPSED_TIME=true'"

# govuln aliases

alias govuln_install="go install golang.org/x/vuln/cmd/govulncheck@latest"
alias govuln_run="govulncheck ./..."

# go_mod_outdated aliases

alias gomodoutdated_install="go install github.com/psampaz/go-mod-outdated@latest"
alias gomodoutdated_run="go list -u -m -json all | go-mod-outdated"

# goreleaser aliases

alias goreleaser_install="go install github.com/goreleaser/goreleaser@latest"
alias goreleaser_init="goreleaser init"
alias goreleaser_build_local="goreleaser build --single-target --clean --snapshot"
alias goreleaser_release="goreleaser release --clean -f conf/goreleaser.yml"
alias goreleaser_release_with_ko="goreleaser release --clean -f conf/goreleaser_v2.yml"

# ko.build aliases

alias kobuild_local="ko build -L ."
alias kobuild_gitlab="ko build -B ."

# test aliases
alias gotest_sonarqube="go test ./... -short -coverprofile dist/coverage.out"

# sonar aliases
alias go_on_sonarcloud="open -a \"Google Chrome\" \"https://sonarcloud.io/project/overview?id=yodamad-github_demoi-ci-go\""
alias go_on_sonar="open -a \"Google Chrome\" \"https://aphrodite.yodamad.fr/dashboard?id=demo-ci\""
