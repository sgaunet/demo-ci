echo "echo \"💉 Sourcing...\"" >> ~/.zshrc

chmod u+x zsh/demo-ci.source
cat zsh/demo-ci.source >> ~/.zshrc

chmod u+x zsh/mvt.source
cat zsh/mvt.source >> ~/.zshrc

chmod u+x zsh/alt-zshrc.source
cat zsh/alt-zshrc.source >> ~/.zshrc
