# Tooling for Go projects

## Linters

- Revive ([info](md/revive.md))
- GolangCI-lint ([info](md/golangci-lint.md))
- Megalinter ([info](md/megalinter.md))
- SonarQube ([info](md/sonar.md))

## Security

- go-mod-outdated ([info](md/go-mod-outdated.md))
- govulncheck ([info](md/govulncheck.md))

## Package & release

- ko.build ([info](md/ko.build.md))
- goreleaser ([info](md/goreleaser.md))
