FROM mcr.microsoft.com/devcontainers/go:1-1.21-bullseye

# Install Docker CE CLI
RUN apt-get update \
    && apt-get install -y apt-transport-https ca-certificates curl gnupg2 lsb-release \
    && curl -fsSL https://download.docker.com/linux/$(lsb_release -is | tr '[:upper:]' '[:lower:]')/gpg | apt-key add - 2>/dev/null \
    && echo "deb [arch=amd64] https://download.docker.com/linux/$(lsb_release -is | tr '[:upper:]' '[:lower:]') $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list \
    && apt-get update \
    && apt-get install -y docker-ce-cli

USER vscode:vscode

# Install go modules
RUN go install github.com/mgechev/revive@latest \
    && go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.55.0 \
    && go install golang.org/x/vuln/cmd/govulncheck@latest \
    && go install github.com/psampaz/go-mod-outdated@latest \
    && go install github.com/goreleaser/goreleaser@latest \
    && go install github.com/google/ko@latest

# Install sonar-scanner
RUN cd /tmp \
    && wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-5.0.1.3006-linux.zip \
    && unzip sonar-scanner-cli-5.0.1.3006-linux.zip

RUN cd /usr/local/bin && sudo ln -s /tmp/sonar-scanner-5.0.1.3006-linux/bin/sonar-scanner sonar-scanner
